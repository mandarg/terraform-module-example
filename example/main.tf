# some comment

provider "aws" {
  region = "us-west-2"
}

# this calls a module that will create a role
module "role" {
  source = "../module/"

  role_name = "modules-demo"
}

# create an output
output "role_name" {
  value = module.role.role_name
}
