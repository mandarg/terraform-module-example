## [1.9.4](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.9.3...v1.9.4) (2020-08-24)


### Bug Fixes

* **test:** update expected role name ([64d449e](https://gitlab.com/roberto.rivera/terraform-module-example/commit/64d449e11d95d4892afd4f455a263c92bdb07fc3))

## [1.9.3](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.9.2...v1.9.3) (2020-08-18)


### Bug Fixes

* **ci:** remove pwd command ([ddc72d4](https://gitlab.com/roberto.rivera/terraform-module-example/commit/ddc72d40ba3610004df98f40b6b990e8ce514eb4))
* **example:** add a comment ([c5f6901](https://gitlab.com/roberto.rivera/terraform-module-example/commit/c5f69015958003e42eea5ad8735f1eff4001a022))

## [1.9.2](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.9.1...v1.9.2) (2020-08-17)


### Bug Fixes

* pwd before running things ([f40ce17](https://gitlab.com/roberto.rivera/terraform-module-example/commit/f40ce17a5e6b070f4b114737bf117d6ff4ddc16b))

## [1.9.1](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.9.0...v1.9.1) (2020-08-17)


### Bug Fixes

* skip unit testing on tags ([ae61396](https://gitlab.com/roberto.rivera/terraform-module-example/commit/ae613961e348fefda3931f2a9874c5aebfc5e63e))

# [1.9.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.8.0...v1.9.0) (2020-08-17)


### Bug Fixes

* add apt update ([b552b41](https://gitlab.com/roberto.rivera/terraform-module-example/commit/b552b41678fc5e275eb110c1d180f00add35a3d7))
* don't go mod ini. ([7aa28a6](https://gitlab.com/roberto.rivera/terraform-module-example/commit/7aa28a65af3a1b68079e7d51c2098c0254b3a364))
* golang:latest is based on debian/buster ([ba4b0c8](https://gitlab.com/roberto.rivera/terraform-module-example/commit/ba4b0c8a660b9c1f6dc7fc4b0df6c1132ca2154b))
* install terraform before running tests ([0d968fe](https://gitlab.com/roberto.rivera/terraform-module-example/commit/0d968fe8f343f79826bd1914f0a8a7388014a2fc))
* we need unzip too ([54684e0](https://gitlab.com/roberto.rivera/terraform-module-example/commit/54684e0a51b3ba8f86741cbe1ad9bf36ecb8c9e5))


### Features

* add tests. some parameter changes. ([8f2e364](https://gitlab.com/roberto.rivera/terraform-module-example/commit/8f2e3641e18fef544fe5b78b085d68b5daa6287e))

# [1.8.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.7.3...v1.8.0) (2020-08-14)


### Features

* add role id output and comments ([64cb8ac](https://gitlab.com/roberto.rivera/terraform-module-example/commit/64cb8ac031cdded7f59828bd6df320b4dabe1e6f))

## [1.7.3](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.7.2...v1.7.3) (2020-08-14)


### Bug Fixes

* we needed the roles arn ([0cb0fb9](https://gitlab.com/roberto.rivera/terraform-module-example/commit/0cb0fb96e710ef33f514a33eefea36edd9a41d5b))

## [1.7.2](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.7.1...v1.7.2) (2020-08-14)


### Bug Fixes

* forgot to include variables ([5802e55](https://gitlab.com/roberto.rivera/terraform-module-example/commit/5802e55d75305d68d5358f2b19b523d394a613ad))
* use the role_name variable ([a01304c](https://gitlab.com/roberto.rivera/terraform-module-example/commit/a01304ce1630fcc800fe89d5415b89301e140df9))

## [1.7.1](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.7.0...v1.7.1) (2020-08-14)


### Bug Fixes

* forgot to add a description to the vars ([eb664c9](https://gitlab.com/roberto.rivera/terraform-module-example/commit/eb664c9337347cb87ea8b512a25b50ab961d0f5d))

# [1.7.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.6.2...v1.7.0) (2020-08-14)


### Bug Fixes

* chmod x terraform-docs ([c8e992c](https://gitlab.com/roberto.rivera/terraform-module-example/commit/c8e992c7f7bb9adb0bf2756ed54000762577138b))


### Features

* add support for terraform-docs ([59487fe](https://gitlab.com/roberto.rivera/terraform-module-example/commit/59487fe5ea98c14e7396b48ded718fd05ad19058))

## [1.6.2](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.6.1...v1.6.2) (2020-08-14)


### Bug Fixes

* reorder semantic-release plugins ([96fe953](https://gitlab.com/roberto.rivera/terraform-module-example/commit/96fe953a34c1b5975e27e0e31c0fbb9d46da3403))

## [1.6.1](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.6.0...v1.6.1) (2020-08-14)


### Bug Fixes

* sed done properly ([29bee78](https://gitlab.com/roberto.rivera/terraform-module-example/commit/29bee7853a765a573530b301bbd77927bd8d2e1c))

# [1.6.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.5.0...v1.6.0) (2020-08-14)


### Bug Fixes

* update sed command ([8e322d3](https://gitlab.com/roberto.rivera/terraform-module-example/commit/8e322d3f81850cf4ba6d098d1990ece99f916587))
* use different separator ([ee2d981](https://gitlab.com/roberto.rivera/terraform-module-example/commit/ee2d98184de3845eabe349e5fb4f333e3c36994c))


### Features

* update module reference ([4155351](https://gitlab.com/roberto.rivera/terraform-module-example/commit/41553514a0eb499734dd7d9242f23ffa05263559))

# [1.6.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.5.0...v1.6.0) (2020-08-14)


### Bug Fixes

* update sed command ([8e322d3](https://gitlab.com/roberto.rivera/terraform-module-example/commit/8e322d3f81850cf4ba6d098d1990ece99f916587))


### Features

* update module reference ([4155351](https://gitlab.com/roberto.rivera/terraform-module-example/commit/41553514a0eb499734dd7d9242f23ffa05263559))

# [1.6.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.5.0...v1.6.0) (2020-08-14)


### Bug Fixes

* update sed command ([8e322d3](https://gitlab.com/roberto.rivera/terraform-module-example/commit/8e322d3f81850cf4ba6d098d1990ece99f916587))


### Features

* update module reference ([4155351](https://gitlab.com/roberto.rivera/terraform-module-example/commit/41553514a0eb499734dd7d9242f23ffa05263559))

# [1.6.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.5.0...v1.6.0) (2020-08-14)


### Bug Fixes

* update sed command ([8e322d3](https://gitlab.com/roberto.rivera/terraform-module-example/commit/8e322d3f81850cf4ba6d098d1990ece99f916587))


### Features

* update module reference ([4155351](https://gitlab.com/roberto.rivera/terraform-module-example/commit/41553514a0eb499734dd7d9242f23ffa05263559))

# [1.6.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.5.0...v1.6.0) (2020-08-14)


### Features

* update module reference ([4155351](https://gitlab.com/roberto.rivera/terraform-module-example/commit/41553514a0eb499734dd7d9242f23ffa05263559))

# [1.6.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.5.0...v1.6.0) (2020-08-14)


### Features

* update module reference ([4155351](https://gitlab.com/roberto.rivera/terraform-module-example/commit/41553514a0eb499734dd7d9242f23ffa05263559))

# [1.6.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.5.0...v1.6.0) (2020-08-14)


### Features

* update module reference ([4155351](https://gitlab.com/roberto.rivera/terraform-module-example/commit/41553514a0eb499734dd7d9242f23ffa05263559))

# [1.5.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.4.0...v1.5.0) (2020-08-14)


### Features

* add example to docs ([99bd739](https://gitlab.com/roberto.rivera/terraform-module-example/commit/99bd739e82cd95babb124fbde54202dddd5bf527))

# [1.4.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.3.2...v1.4.0) (2020-08-14)


### Features

* add some proper linting ([076d985](https://gitlab.com/roberto.rivera/terraform-module-example/commit/076d9857b345ce168b645ac5dc82ed81a1c18865))

## [1.3.2](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.3.1...v1.3.2) (2020-08-14)


### Bug Fixes

* **ci:** make zip junk the folder ([5840cc1](https://gitlab.com/roberto.rivera/terraform-module-example/commit/5840cc1f290bb4e2ec43d7644161e7fe67da3b9f))

## [1.3.1](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.3.0...v1.3.1) (2020-08-14)


### Bug Fixes

* **ci:** silence apk installs ([19b9fff](https://gitlab.com/roberto.rivera/terraform-module-example/commit/19b9fffb7e4a7f331a0a558c93a7069c97b982e4))

# [1.3.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.2.0...v1.3.0) (2020-08-14)


### Features

* upload to s3 ([2f04f18](https://gitlab.com/roberto.rivera/terraform-module-example/commit/2f04f18e1ba05bb849bfd85f2c56f81c99c27bb5))

# [1.2.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.1.0...v1.2.0) (2020-08-14)


### Features

* display zip contents ([bca0758](https://gitlab.com/roberto.rivera/terraform-module-example/commit/bca0758790a6baf06133c17817b6c6e42e1a019c))

# [1.1.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.0.0...v1.1.0) (2020-08-14)


### Bug Fixes

* clean up semantic-release config ([9966b0b](https://gitlab.com/roberto.rivera/terraform-module-example/commit/9966b0bbb9d366a48ee36a818115da85b4248262))
* mock zip file name ([fc7e56a](https://gitlab.com/roberto.rivera/terraform-module-example/commit/fc7e56a67a7a8f2e4ca994b5d24edb5bc0a00ab8))
* update prepare command ([8767b99](https://gitlab.com/roberto.rivera/terraform-module-example/commit/8767b990247dc9e5227387ab15e91f7ba1e5edd5))


### Features

* move semantic-release config to .releaserc ([7e593a6](https://gitlab.com/roberto.rivera/terraform-module-example/commit/7e593a639398239003ad3d8dbbfae032ad577f80))
* use alpine for semantic release ([921f733](https://gitlab.com/roberto.rivera/terraform-module-example/commit/921f733d72d1d690468e54677b79db7aa04ef7c4))

# [1.1.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.0.0...v1.1.0) (2020-08-14)


### Bug Fixes

* clean up semantic-release config ([9966b0b](https://gitlab.com/roberto.rivera/terraform-module-example/commit/9966b0bbb9d366a48ee36a818115da85b4248262))
* update prepare command ([8767b99](https://gitlab.com/roberto.rivera/terraform-module-example/commit/8767b990247dc9e5227387ab15e91f7ba1e5edd5))


### Features

* move semantic-release config to .releaserc ([7e593a6](https://gitlab.com/roberto.rivera/terraform-module-example/commit/7e593a639398239003ad3d8dbbfae032ad577f80))
* use alpine for semantic release ([921f733](https://gitlab.com/roberto.rivera/terraform-module-example/commit/921f733d72d1d690468e54677b79db7aa04ef7c4))

# [1.1.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.0.0...v1.1.0) (2020-08-14)


### Bug Fixes

* clean up semantic-release config ([9966b0b](https://gitlab.com/roberto.rivera/terraform-module-example/commit/9966b0bbb9d366a48ee36a818115da85b4248262))
* update prepare command ([8767b99](https://gitlab.com/roberto.rivera/terraform-module-example/commit/8767b990247dc9e5227387ab15e91f7ba1e5edd5))


### Features

* move semantic-release config to .releaserc ([7e593a6](https://gitlab.com/roberto.rivera/terraform-module-example/commit/7e593a639398239003ad3d8dbbfae032ad577f80))
* use alpine for semantic release ([921f733](https://gitlab.com/roberto.rivera/terraform-module-example/commit/921f733d72d1d690468e54677b79db7aa04ef7c4))

# [1.1.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.0.0...v1.1.0) (2020-08-14)


### Bug Fixes

* clean up semantic-release config ([9966b0b](https://gitlab.com/roberto.rivera/terraform-module-example/commit/9966b0bbb9d366a48ee36a818115da85b4248262))


### Features

* move semantic-release config to .releaserc ([7e593a6](https://gitlab.com/roberto.rivera/terraform-module-example/commit/7e593a639398239003ad3d8dbbfae032ad577f80))
* use alpine for semantic release ([921f733](https://gitlab.com/roberto.rivera/terraform-module-example/commit/921f733d72d1d690468e54677b79db7aa04ef7c4))

# [1.1.0](https://gitlab.com/roberto.rivera/terraform-module-example/compare/v1.0.0...v1.1.0) (2020-08-14)


### Bug Fixes

* clean up semantic-release config ([9966b0b](https://gitlab.com/roberto.rivera/terraform-module-example/commit/9966b0bbb9d366a48ee36a818115da85b4248262))


### Features

* move semantic-release config to .releaserc ([7e593a6](https://gitlab.com/roberto.rivera/terraform-module-example/commit/7e593a639398239003ad3d8dbbfae032ad577f80))
* use alpine for semantic release ([921f733](https://gitlab.com/roberto.rivera/terraform-module-example/commit/921f733d72d1d690468e54677b79db7aa04ef7c4))

# 1.0.0 (2020-07-31)


### Bug Fixes

* add release.config.js ([1dc1401](https://gitlab.com/roberto.rivera/terraform-module-example/commit/1dc1401aee81da59775ecb14b0b5bcde712aa476))
* remove @semantic-release/git ([18a6d07](https://gitlab.com/roberto.rivera/terraform-module-example/commit/18a6d07a96f44ef0117e32f5746137c77d860cca))


* Initial commit ([975e49e](https://gitlab.com/roberto.rivera/terraform-module-example/commit/975e49e16a2ce9eacb8a5b9e1f5393813f64aabf))
