output "role_name" {
  value       = aws_iam_role.role.name
  description = "the role's name"
}

output "role_arn" {
  value       = aws_iam_role.role.arn
  description = "the role's arn"
}

output "role_id" {
  value       = aws_iam_role.role.id
  description = "the role's id"
}
